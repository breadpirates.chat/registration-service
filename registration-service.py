#!/usr/bin/python3

import cherrypy
import psycopg2
from uuid import uuid4
import json
from jinja2 import FileSystemLoader
from jinja2.environment import Environment
from urllib.parse import urlparse
from synapse._scripts.register_new_matrix_user import register_new_user
import requests
import re
from sys import argv, exit
from json import loads as json_loads

if len(argv) != 2:
    print("python registration-service.py <Path to Config File>")
    exit(1)

config = json_loads(open(argv[1], "r").read())

REGISTRATION_SHARED_SECRET = config['registration_shared_secret']

if config['invite']['enabled']:
    from matrix_client.api import MatrixHttpApi
    matrix = MatrixHttpApi(
        config['invite']['homeserver_url'],
        token=config['invite']['access_token']
    )


dbcon = psycopg2.connect("dbname=registration user=registration password=registration host=127.0.0.1")

jinja = Environment()
jinja.loader = FileSystemLoader("./templates")

username_regex = re.compile("[^a-z0-9\._=\-\/]")

def get_profile_url(mxc_url):
    parsed = urlparse(mxc_url)
    hs_host = parsed.netloc
    media_id = parsed.path[1::]
    return f"https://breadpirates.chat/_matrix/media/r0/thumbnail/{hs_host}/{media_id}?width=256&height=256&method=crop"

class RegistrationService(object):
    @cherrypy.expose
    def index(self, invite_token=""):
        if invite_token == "":
            tmpl = jinja.get_template("error.jinja")
            return tmpl.render(error="No invite token was specified.")
        cursor = dbcon.cursor()
        cursor.execute("SELECT * FROM tokens WHERE token=%s", (invite_token,))
        token = cursor.fetchone()
        cursor.close()
        if not token:
            tmpl = jinja.get_template("error.jinja")
            return tmpl.render(error="The specified registration token does not exist.")
        
        localpart = token[1]
        display_name = token[2]
        profile_url = token[3]
        count = int(token[4])
        remaining = int(token[5])

        print(int(remaining))

        if remaining <= 0:
            tmpl = jinja.get_template("error.jinja")
            return tmpl.render(error="The specified registration token has no registrations left. Please ask for a new one.")
        
        if display_name == "":
            display_name = localpart
        
        if profile_url != "":
            profile_url = get_profile_url(profile_url)

        tmpl = jinja.get_template("register.jinja")
        return tmpl.render(
            name=display_name,
            profile_url=profile_url,
            token=invite_token
        )
    
    @cherrypy.expose
    @cherrypy.tools.json_in()
    def request_token(self):
        try:
            body = cherrypy.request.json
            uuid = str(uuid4())

            localpart = body['localpart']
            count = body['count']
            
            vanity_data = requests.get(f"https://breadpirates.chat/_matrix/client/r0/profile/@{localpart}:breadpirates.chat").json()
            if 'displayname' in vanity_data:
                display_name = vanity_data['displayname']
            else:
                display_name = ""
            
            if 'avatar_url' in vanity_data:
                profile_url = vanity_data['avatar_url']
            else:
                profile_url = ""
        
            cursor = dbcon.cursor()
            cursor.execute("INSERT INTO tokens (token, localpart, display_name, profile_url, created_at, count, remaining) VALUES (%s, %s, %s, %s, NOW(), %s, %s)", (uuid, localpart, display_name, profile_url, count, count))
            dbcon.commit()
            return json.dumps({
                'error': False,
                'url': f'https://breadpirates.chat/registration?invite_token={uuid}'
            })
        except Exception as e:
            return json.dumps({
                'error': str(e)
            })
    
    @cherrypy.expose
    def do_registration(self, token, username, password, password2):
        err_tmpl = jinja.get_template("error.jinja")

        cursor = dbcon.cursor()
        cursor.execute("SELECT * FROM tokens WHERE token=%s", (token,))
        res = cursor.fetchone()
        if not res:
            return err_tmpl.render(error="Registration token is not valid.")

        localpart = username.lower()
        remaining = int(res[5])

        if remaining <= 0:
            return err_tmpl.render(error="The specified token has no registrations left. Please ask for a new one.")

        if password != password2:
            return err_tmpl.render(error="Password fields do not match.")
        
        if len(password) < 12:
            return err_tmpl.render(error="Password is too short. It needs to be at least 12 characters long.")
        
        if len(password) > 512:
            return err_tmpl.render(error="Password is too long. It needs to be shorter than 512 characters long. (Do you really need a password that long?)")

        if len(localpart) < 3:
            return err_tmpl.render(error="Username is too short. It needs to be at least 3 characters long.")

        if username_regex.search(localpart):
            return err_tmpl.render(error="Username contains invalid characters. Please choose a different one.")
        
        exists_check = requests.get(f"https://breadpirates.chat/_matrix/client/r0/profile/@{localpart}:breadpirates.chat").json()
        if 'errcode' not in exists_check or exists_check['errcode'] != "M_NOT_FOUND":
            return err_tmpl.render(error=f"The requested username, {localpart}, already exists. Try again with a different one.")
        
        register_new_user(
            localpart,
            password,
            "http://localhost:8008",
            REGISTRATION_SHARED_SECRET,
            False,
            None
        )

        print(int(remaining))

        remaining_insert = remaining - 1

        print(int(remaining_insert))

        cursor.execute("INSERT INTO registrations (token, localpart, created_at) VALUES (%s, %s, NOW())", (token, localpart))
        cursor.execute("UPDATE tokens SET remaining=%s WHERE token=%s", (remaining_insert, token))
        dbcon.commit()
        cursor.close()

        if config['invite']['enabled']:
            for room in config['invite']['rooms']:
                matrix.invite_user(room, f"@{localpart}:{config['invite']['homeserver']}")

        return jinja.get_template("success.jinja").render()

cherrypy.config.update({'server.socket_port': 8009})
cherrypy.tree.mount(RegistrationService(), "/registration")

cherrypy.engine.start()
cherrypy.engine.block()